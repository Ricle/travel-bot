from django.contrib import admin

from promo.models import Promo
from promo.tasks import make_mailing
from .models import User, TourFilter, TourDetail, TourCallback, MonitoringText



class UserMonitoringFilter(admin.SimpleListFilter):
    title = 'Категории'
    parameter_name = 'some'

    def lookups(self, request, model_admin):
        return [
            ('monitoring', 'С мониторингом'),
            ('not monitoring', 'Без мониторинга')
        ]

    def queryset(self, request, queryset):
        if self.value() == 'not monitoring':
            return queryset.filter(on_monitoring=[])
        if self.value() == 'monitoring':
            return queryset.exclude(on_monitoring=[])


def make_mailing_action(promo):
    """Action factory function."""

    def mail_this(modeladmin, request, queryset):
        """Directly action funcion."""
        make_mailing.delay(promo[0], tuple(queryset.values_list('chat_id', flat=True)))
        modeladmin.message_user(request, f'Рассылка {promo[1]} успешно запущена.')

    mail_this.short_description = f'Разослать акцию: {promo[1]}'
    mail_this.__name__ = f'Разослать акцию: {promo[1]}'

    return mail_this


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_filter = (UserMonitoringFilter,)

    def get_list_display(self, request):
        if request.user.is_superuser:
            list_display = ("chat_id", "role", "state", "on_monitoring")
        else:
            list_display = ("chat_id", "role", "state", "on_monitoring")
        return list_display

    def get_actions(self, request):
        actions = super().get_actions(request)

        for promo in Promo.objects.values_list('id', 'title'):
            action = make_mailing_action(promo)
            actions[action.__name__] = (action,
                                        action.__name__,
                                        action.short_description)
        return actions


@admin.register(TourFilter)
class TourFilterAdmin(admin.ModelAdmin):

    def get_list_display(self, request):
        if request.user.is_superuser:
            list_display = ("pk", "user", "state")
        else:
            list_display = ("pk", "user", "state")
        return list_display

    search_fields = ('pk', )


@admin.register(TourDetail)
class TourDetailAdmin(admin.ModelAdmin):
    list_display = ("key", "tour_id")

    search_fields = ('key', "tour_id")


@admin.register(MonitoringText)
class MonitoringTextAdmin(admin.ModelAdmin):
    list_display = ("user", "filter")

    search_fields = ('user', )


admin.site.register(TourCallback)
# admin.site.register(MonitoringText)
