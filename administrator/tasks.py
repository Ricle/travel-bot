from __future__ import absolute_import, unicode_literals

from loguru import logger
from Otpusk.celery import app
from tbot.customer import CustomerBot
from .models import TourFilter, User, MonitoringText
from tbot.api_callback import new_search, make_text, check_api, periodic_search
from tbot.bot_tools import get_lang_text as _l
from tbot.bot_tools import text_splitter
# from telebot import util


@app.task
def generate_tours(*args):
    """generate tours and send it to user"""
    CBot = CustomerBot()
    tour_pk, user_id = args

    text = _l(user_id, 'system0')
    message = CBot.bot.send_message(user_id, text)

    tour_filter = TourFilter.objects.get(pk=tour_pk)
    logger.info(tour_filter)
    result = new_search(tour_filter)
    if result:
        CBot.delete_msg(user_id, message.message_id)
        CBot.generate_search_result(user_id, tour_filter.pk)
    else:
        CBot.delete_msg(user_id, message.message_id)
        text = _l(user_id, 'system1')
        CBot.bot.send_message(user_id, text)


@app.task
def hour_check_tours():
    """method that checks users every hour if they have tour to monitoring"""
    users = User.objects.filter(role='Customer')
    logger.info(users)
    for user in users:
        real_hour_check_in.delay(user.pk)


@app.task
def real_hour_check_in(user_pk):
    """method that checks tours"""
    CBot = CustomerBot()
    user = User.objects.get(chat_id=user_pk)
    try:
        tour_filter = TourFilter.objects.get(pk=user.on_monitoring[-1])
        logger.info(tour_filter)
        tour_callback_, status = periodic_search(tour_filter)
        logger.info(tour_callback_)
        logger.info(status)
        monitoring_text = MonitoringText.objects.filter(user=user, filter=tour_filter).last()
        if not tour_callback_:      # if obj=None - monitoring END
            user.on_monitoring = []
            user.save()
            text = monitoring_text.text

            monitoring_text.delete()    # delete text from db
            CBot.bot.send_message(user.chat_id, text)
            text1 = _l(user.chat_id, 'monitor4')
            keyboard = CBot.Keyboards.main_menu(user.chat_id)
            return CBot.bot.send_message(user.chat_id, text1, reply_markup=keyboard)

        if status or tour_callback_.pk != monitoring_text.callback.pk:
            # if status=True - new data in filter - change text
            # OR if callback was in DB and return FALSE in status
            monitoring_text.callback = tour_callback_
            text_ = "\n\n"
            text_ += f"[Тур ID] {tour_callback_.tour.tour_id}\n"
            text_ += f"<b>[Новая стоимость тура] {tour_callback_.price}</b>\n"
            text_ += f"[Название оператора] {tour_callback_.operator}\n"
            text_ += f"[Места на рейсе туда] <b>{tour_callback_.place_from}</b>\n"
            text_ += f"[Места на рейсе обратно] <b>{tour_callback_.place_to}</b>\n"

            monitoring_text.text += text_
            monitoring_text.save()
            logger.info('NEW_text_save')
            keyboard = CBot.Keyboards.new_monitoring_flow(tour_filter.pk, user_pk)

            text_splitter(CBot, user.chat_id, monitoring_text.text, keyboard)   # send spliting messages

            # splitted_text = util.split_string(monitoring_text.text, 3000)
            # logger.info(len(f'{splitted_text=}'))
            # for index, text in enumerate(splitted_text, start=1):
            #     CBot.bot.send_message(user.chat_id, text)
            #     if index == len(splitted_text):
            #         return CBot.bot.edit_message_text(user.chat_id, text, reply_markup=keyboard)
    except:
        pass

