import i18n
from django.contrib.postgres.fields import ArrayField
from telebot import types
from tbot.tbot import tBot
from django.db import models

ROLES = [
    ("Administrator", "Administrator"),
    ("Customer", "Customer"),
]

LANGUAGE_CODE = [
    ('ru', 'ru'),
]

USER_STATES = [
    ('start', 'start'),
    ('name', 'name'),
    ('phone', 'phone'),
    ('agency', 'agency'),
    ('regular', 'regular'),
    ('tour_filter', 'tour_filter'),
    ('edit_menu', 'edit_menu'),
    ('edit_step', 'edit_step'),
]


class User(models.Model):
    """user model"""
    chat_id = models.CharField("users Chat_id", max_length=100, primary_key=True)
    role = models.CharField("users role", max_length=30, default='Customer', choices=ROLES)
    name = models.CharField("users name", max_length=100, null=True, blank=True)
    username = models.CharField("username from Telegram", max_length=100, blank=True, null=True)
    phone = models.CharField("user's number", max_length=30, blank=True, null=True)
    agency = models.CharField("user's agency", max_length=500, blank=True, null=True)
    language_code = models.CharField("language", max_length=2, default='ru', choices=LANGUAGE_CODE)
    state = models.CharField("state", default='start', max_length=200, choices=USER_STATES)
    accept = models.BooleanField('access to flow', default=False)
    on_monitoring = ArrayField(models.CharField('monitoring ON', max_length=100, null=True, blank=True,
                                                default=False), null=True, blank=True, default=list)

    def __str__(self):
        return f'User - {self.chat_id}'

    def save(self, *args, **kwargs):
        """send message to user that take role Administrator from admin-site"""
        if self.role == "Administrator":
            i18n.set('locale', f'{self.language_code}')
            text = i18n.t(f'translate.admin0_0')
            Tbot = tBot()
            Tbot.bot.send_message(self.chat_id, text, reply_markup=types.ReplyKeyboardRemove())
            self.accept = True
            super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)

    class Meta:
        ordering = ['pk']
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class TourFilter(models.Model):
    """tour model"""
    CITY = [
        ('2014', 'Киев'),
        ('1225', 'Харьков'),
        ('30419', 'Днепр'),
        ('449', 'Одесса'),
        ('1745', 'Львов'),
        ('1212', 'Запорожье'),
        ('437', 'Николаев'),
        ('304', 'Херсон'),
    ]

    MEAL = [
        ('512', 'AI'),
        ('388', 'BB'),
        ('498', 'FB'),
        ('496', 'HB'),
        ('560', 'UAI'),
        ('1956', 'RO'),
    ]
    CURRENCY = [
        ('2', 'UAH'),
        ('1', 'USD'),
    ]
    TOUR_STATES = [
        ('start', 'start'),
        ('from_city', 'from_city'),
        ('country', 'country'),
        ('to_city', 'to_city'),
        ('date_from', 'date_from'),
        ('date_till', 'date_till'),
        ('nights_from', 'nights_from'),
        ('nights_to', 'nights_to'),
        ('adult_amount', 'adult_amount'),
        ('child_amount', 'child_amount'),
        ('child_counter|1', 'child_counter|1'),
        ('child_counter|2', 'child_counter|2'),
        ('child_counter|3', 'child_counter|3'),
        ('hotel_rating', 'hotel_rating'),
        ('hotel', 'hotel'),
        ('meal_type', 'meal_type'),
        ('done', 'done'),
    ]
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    from_city = models.CharField("tour from city", max_length=50, blank=True, null=True, choices=CITY)
    country = models.JSONField("country", default=dict, blank=True, null=True)
    to_city = models.JSONField("region", default=dict, blank=True, null=True)
    date_from = models.DateField('date from', blank=True, null=True)
    date_till = models.DateField('date till', blank=True, null=True)
    nights_from = models.PositiveSmallIntegerField('nights from', blank=True, null=True)
    nights_to = models.PositiveSmallIntegerField('nights to', blank=True, null=True)
    adult_amount = models.PositiveSmallIntegerField('adults', blank=True, null=True)
    child_amount = models.JSONField(default=dict, blank=True, null=True)
    hotel_rating = models.CharField("hotels rating", max_length=20, blank=True, null=True)
    star2 = models.BooleanField(default=False)
    star3 = models.BooleanField(default=False)
    star4 = models.BooleanField(default=False)
    star5 = models.BooleanField(default=False)
    hotel = models.JSONField("hotel", default=dict, blank=True, null=True)
    meal_type = models.CharField("meal type", max_length=50, blank=True, null=True)
    AI = models.BooleanField(default=False)
    BB = models.BooleanField(default=False)
    FB = models.BooleanField(default=False)
    HB = models.BooleanField(default=False)
    UAI = models.BooleanField(default=False)
    RO = models.BooleanField(default=False)
    currency = models.CharField("currency", max_length=50, blank=True, null=True, choices=CURRENCY)
    state = models.CharField("state", max_length=50, default='start', choices=TOUR_STATES)

    def str_hotel_stars(self):
        """return string view of hotel stars"""
        if not self.hotel_rating:
            stars = '3*, 4*, 5*'
        else:
            stars = ','.join([i + '*' for i in self.hotel_rating.split(':')]).replace('78', '5')
            stars = stars.replace('7', '2')
        return stars

    def str_meal_type(self):
        """return string view of meal type"""
        if not self.meal_type:
            meal = 'любой'
        else:
            meal = ', '.join([j for i, j in TourFilter.MEAL if getattr(self, f"{j}") is True])
        return meal

    def str_child_ages(self):
        """return string view of child ages"""
        if not self.child_amount:
            ages = '-'
        else:
            ages = ', '.join([self.child_amount[str(i)] for i in range(1, len(self.child_amount) + 1)])
        return ages

    def str_to_city(self):
        """return string view of to_city"""
        if not self.to_city:
            city = ''
        else:
            city = self.to_city['name']
        return city

    def str_to_hotel(self):
        """return string view of hotel"""
        if not self.hotel:
            hotel = ''
        else:
            hotel = self.hotel['name']
        return hotel

    class Meta:
        ordering = ['pk']
        verbose_name = 'Filter'
        verbose_name_plural = 'Filters'


class TourDetail(models.Model):
    """model that contain details about tour"""

    key = models.CharField('unique tour key', max_length=100, primary_key=True)
    filters = models.ManyToManyField(TourFilter)
    users = models.ManyToManyField(User)
    tour_id = models.CharField('tour_id', max_length=20, blank=True, null=True)
    active = models.BooleanField('status', default=False)
    order_int = models.PositiveBigIntegerField('unique integer for ordering', default=0)

    class Meta:
        ordering = ['order_int']
        verbose_name = 'TourDetail'
        verbose_name_plural = 'TourDetails'


class TourCallback(models.Model):
    """data that foreign for TourDetail"""
    tour = models.ForeignKey(TourDetail, on_delete=models.CASCADE, related_name='t_d')
    flights = models.CharField('flight_date_from - flight_date_to', max_length=200, blank=True, null=True)
    duration_nights = models.CharField('duration_nights', max_length=2, blank=True, null=True)
    hotel = models.CharField('hotel name and rating', max_length=200, blank=True, null=True)
    meal_type = models.CharField('meal_type', max_length=10, blank=True, null=True)
    room_type = models.CharField('room_type', max_length=100, blank=True, null=True)
    accomodation = models.CharField('accomodation', max_length=100, blank=True, null=True)
    price = models.CharField('price', max_length=10, blank=True, null=True)
    operator = models.CharField('operator', max_length=200, blank=True, null=True)
    place_from = models.CharField('place_from', max_length=50, blank=True, null=True)
    place_to = models.CharField('place_from', max_length=50, blank=True, null=True)

    region = models.JSONField("region", default=dict, blank=True, null=True)
    date_on = models.DateField('date from', blank=True, null=True)
    hotel_dict = models.JSONField("hotel", default=dict, blank=True, null=True)

    class Meta:
        ordering = ['pk']
        verbose_name = 'TourCallback'
        verbose_name_plural = 'TourCallbacks'


class MonitoringText(models.Model):
    """monitoring text container"""
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    filter = models.ForeignKey('TourFilter', on_delete=models.CASCADE)
    text = models.TextField('text to user', blank=True, null=True, default=None)
    callback = models.ForeignKey('TourCallback', on_delete=models.CASCADE)

    class Meta:
        ordering = ['pk']
        verbose_name = 'MonitoringText'
        verbose_name_plural = 'MonitoringText'















