**Чат-бот позволяет Отслеживание изменения стоимости туров на сайте https://www.ittour.com.ua/ по заданым параметрам**

Settings before deploy service:
    1. Put your own SSL certificate - put it to the right place (django.conf, lines 4,5) 
and !!! comment CERTBOT container in docker-compose.yml file
    2. in ./docker/nginx/django.conf - replace YOUR_DOMAIN in LINE 5,6,24,69 to actual / leave commented lines
    3. in ./docker-compose.yml - line 26 - `POSTGRES_PASSWORD: pass` - replace `pass` - to strong password.
    4. in ./SPYGLASS/setting.py - (DEBUG = False), (DEV_MODE = False), (`ALLOWED_HOSTS = ['*']` - replace star to your IP and/or host)
    5. in ./SPYGLASS/setting.py - `DATABASES =` same as in docker-compose.yml!

Deploy:
1. build docker-compose
2. in first start in python container: makemigrations / migrate / createsuperuser / collectstatic


go_to https://your_ip/admin - login as superuser.
In Admin:
1. TBOT - Bot_configs -- add new / place your BOT token in TOKEN and mark "is active"
2. TBOT - Api tokens -- put IT-tour API token
3. PERIODIC TASKS - Crontabs -- add one with this settings: 0 9-20 * * * (m/h/dM/MY/d) Europe/Kiev
4. PERIODIC TASKS - Periodic tasks -- add new task: 
   Name: - MAIN
   Task (registered): administrator.tasks.hour_check_tours
   Schedule - Crontab Schedule: pick "0 9-20 * * * (m/h/dM/MY/d) Europe/Kiev"
   SAVE

You MUST add one Administrator-role user before start your bot.

All users, you add from admin-site must have:
1. Users Chat_id
2. State - regular
3. Access to flow - True
   
   
after that - reload docker container