import json
import emoji
import time

from enum import Enum
from loguru import logger
from administrator.models import User, TourFilter, TourDetail, TourCallback, MonitoringText
from promo.models import Promo
from dateutil.relativedelta import relativedelta
from django.core.paginator import Paginator
from django.utils import timezone
from telebot import types
from telebot.apihelper import ApiException
from telegram_bot_calendar import WMonthTelegramCalendar

from .bot_tools import get_lang_text as _l, text_splitter
from .tbot import tBot
from .api_callback import new_search, make_text, check_api


class CustomerBot(tBot):
    """main methods class for CUSTOMER"""

    CALL_DATA = ['from_city', 'country', 'to_city', 'date_from', 'date_till', 'nights_from',
                 'nights_to', 'adults', 'childrens', 'hotel_class', 'hotel_name', 'meal', 'currency']

    def user_new_make(self, message):
        """method that take id, make new user in DB and send welcome screen"""
        user, var = User.objects.get_or_create(chat_id=message.from_user.id)
        if not var:
            user.state = 'start'
            user.save()
        text = _l(message.from_user.id, 'start0').format(message.from_user.id)
        return self.bot.send_message(message.from_user.id, text)

    def user_set_name(self, message, user):
        """method when user take name and ask about phone"""
        if message.text:
            user.name = message.text
            user.state = 'name'
            user.username = message.from_user.username
            user.save()
            text = _l(message.from_user.id, 'reg0')
            return self.bot.send_message(message.from_user.id, text)
        else:
            self.user_new_make(message)

    def user_set_phone(self, message, user):
        """method when user take phone and ask about agency"""
        if message.text.isdigit():
            user.phone = message.text
            user.state = 'phone'
            user.save()
            User.objects.filter(chat_id=message.from_user.id).update(phone=message.text, state='phone')
            text = _l(message.from_user.id, 'reg1')
        else:
            user.state = 'name'
            user.save()
            text = _l(message.from_user.id, 'reg0')
        return self.bot.send_message(message.from_user.id, text)

    def user_set_agency(self, message, user):
        """method when user take agency and send wait message"""
        if message.text:
            user.agency = message.text
            user.state = 'agency'
            user.save()
            # User.objects.filter(chat_id=message.from_user.id).update(agency=message.text, state='agency')
            text = _l(message.from_user.id, 'reg2')
            ################ msg to admin ###
            text_a = _l(message.from_user.id, 'admin1_0').format(user.name, user.phone, user.agency)
            b_text_a = [_l(message.from_user.id, f'admin1_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.admin_buttons(b_text_a, user.chat_id)
            admin = User.objects.filter(role='Administrator').last()
            self.bot.send_message(admin.chat_id, text_a, reply_markup=keyboard)
        else:
            User.objects.filter(chat_id=message.from_user.id).update(state='phone')
            text = _l(message.from_user.id, 'reg1')
        return self.bot.send_message(message.from_user.id, text)

    def user_main_menu(self, message, back=None):
        """method send main menu to user"""
        User.objects.filter(chat_id=message.from_user.id).update(state='regular')
        text = _l(message.from_user.id, 'menu0_0')
        keyboard = self.Keyboards.main_menu(message.from_user.id)
        if not back:
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
        else:
            return self.bot.edit_message_text(chat_id=message.from_user.id, text=text,
                                              message_id=message.message.message_id, reply_markup=keyboard)

    def tour_set_new(self, message, user, edit=None):
        """method start new tour make"""
        status = check_api()  # check API status
        if not status:
            text = _l(message.from_user.id, 'menu0_6')
            return self.bot.send_message(message.from_user.id, text)
        else:
            if not edit:
                TourFilter.objects.create(user=user)
                user.state = 'tour_filter'
                user.save()
            else:
                pass
            text = _l(message.from_user.id, 'tour0_0')
            b_text = _l(message.from_user.id, 'menu1')
            keyboard = self.Keyboards.tour_city(b_text)
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)

    def tour_set_from_city(self, call, tour, user, edit=None):
        """method set city from and ask country"""
        _, city = call.data.split('|')
        tour.from_city = city
        tour.state = 'from_city'
        tour.save()
        # r_text = _l(call.from_user.id, 'menu0')
        # r_b_text = [_l(call.from_user.id, f'menu{i}') for i in [1, 2]]
        # r_keyboard = self.Keyboards.back_and_menu(r_b_text)
        # self.bot.send_message(call.from_user.id, r_text, reply_markup=r_keyboard)
        if not edit:
            i_text = _l(call.from_user.id, 'tour1_0')
            i_b_text = _l(call.from_user.id, 'tour1_1')
            i_keyboard = self.Keyboards.country(i_b_text, user_id=user.chat_id)
            return self.bot.send_message(call.from_user.id, i_text, reply_markup=i_keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_set_country(self, message, tour, user, edit=None):
        """method set country and ask city"""
        _, country_id, country_name = message.text.split('|')
        tour.country = {'id': country_id, 'name': country_name}  # place dict to DB
        tour.state = 'country'
        tour.save()
        if not edit:
            text = _l(message.from_user.id, 'tour2_0')
            b_text = [_l(message.from_user.id, f'tour2_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.to_city(b_text, user_id=user.chat_id)
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
        else:
            tour.to_city = {}
            tour.hotel = {}
            tour.save()
            self.tour_filter_last_msg(message, tour, user)

    def tour_set_to_city(self, message_call, tour, user, edit=None):
        """method set city and ask date_from"""
        try:
            _, city_id, city_name = message_call.text.split('|')
            tour.to_city = {'id': city_id, 'name': city_name}
        except:
            tour.to_city = ''
        tour.state = 'to_city'
        tour.save()
        if not edit:
            text = _l(message_call.from_user.id, 'tour3')
            calendar, step = WMonthTelegramCalendar(locale='ru',
                                                    min_date=timezone.localtime(timezone.now()).date()
                                                             + relativedelta(days=+1)).build()

            keyboard = self.add_menu_to_calendar(user, calendar)       # MENU buttons add to calendar

            return self.bot.send_message(message_call.from_user.id, text, reply_markup=keyboard)
        else:
            tour.hotel = {}
            tour.save()
            self.tour_filter_last_msg(message_call, tour, user)

    def add_menu_to_calendar(self, user, calendar):
        """method generate and add menu buttons to calendar"""
        b = json.loads(calendar)
        keyboard = self.Keyboards.back_and_menu(user.chat_id)
        c = b['inline_keyboard']
        c.append(keyboard.to_dict()['inline_keyboard'][0])
        keyboard_ = json.dumps({'inline_keyboard': c})
        return keyboard_

    def tour_set_date_from(self, call, tour, result, user, edit=None):
        """set date_from and ask date_till"""
        tour.date_from = result
        tour.state = 'date_from'
        tour.save()
        # if not edit or tour.date_till < tour.date_from:
        if not edit:
            # if tour.date_till < tour.date_from:
            #     user.state = 'edit_step'
            #     user.save()
            text = _l(call.from_user.id, 'tour4')
            min_date = tour.date_from
            max_date = tour.date_from + relativedelta(days=+12)  # not bigger that +12 days from date_from
            calendar, step = WMonthTelegramCalendar(locale='ru', min_date=min_date, max_date=max_date).build()

            keyboard = self.add_menu_to_calendar(user, calendar)       # MENU buttons add to calendar

            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_set_date_till(self, call, tour, result, user, edit=None):
        """set date_till and ask nights count from"""
        tour.date_till = result
        tour.state = 'date_till'
        tour.save()
        if not edit:
            text = _l(call.from_user.id, 'tour5')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_set_nights_from(self, message, tour, user, edit=None):
        """set nights from and ask nights for"""
        if message.text.isdigit() and int(message.text) in range(1, 31):
            tour.nights_from = message.text
            tour.state = 'nights_from'
            tour.save()
            if not edit:
                text = _l(message.from_user.id, 'tour6')
                keyboard = self.Keyboards.back_and_menu(user.chat_id)
                return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
            else:
                self.tour_filter_last_msg(message, tour, user)
        else:
            text = _l(message.from_user.id, 'tour5')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)

    def tour_set_nights_to(self, message, tour, user, edit=None):
        """set nights to and ask nights for adults"""
        if message.text.isdigit() and int(message.text) >= tour.nights_from and int(message.text) in range(1, 31):
            tour.nights_to = message.text
            tour.state = 'nights_to'
            tour.save()
            if not edit:
                text = _l(message.from_user.id, 'tour7')
                keyboard = self.Keyboards.adults(user.chat_id)
                return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
            else:
                self.tour_filter_last_msg(message, tour, user)
        else:
            text = _l(message.from_user.id, 'tour6')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)

    def tour_set_adults(self, call, tour, user, edit=None):
        """set adults count and ask childrens count"""
        _, count = call.data.split('|')
        tour.adult_amount = int(count)
        tour.state = 'adult_amount'
        tour.save()
        if not edit:
            text = _l(call.from_user.id, 'tour8_0')
            keyboard = self.Keyboards.childrens(user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_set_childrens(self, call, tour, user, edit=None):
        """set childrens count and ask childrens age or hotel class"""
        _, count = call.data.split('|')
        if int(count) == 0:  # if child count == 0
            tour.state = 'child_amount'
            tour.save()
            if not edit:
                text = _l(call.from_user.id, 'tour9_0')
                b_text = [_l(call.from_user.id, f'tour9_{i}') for i in [1, 2]]
                keyboard = self.Keyboards.hotel_class(b_text, user)
                return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
            else:
                tour.child_amount = {}
                tour.save()
                self.tour_filter_last_msg(call, tour, user)
        else:  # if child more than 0
            dict_ = {i: '' for i in range(1, int(count) + 1)}
            tour.child_amount = dict_
            tour.state = 'child_counter|1'
            tour.save()
            text = _l(call.from_user.id, 'tour8_1').format('первого')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    def tour_set_child_age(self, message, tour, user, edit=None):
        """set child age"""
        _, counter = tour.state.split('|')
        if message.text.isdigit() and int(message.text) in range(0, 17):
            tour.child_amount[counter] = message.text
            tour.save()
            if int(counter) == len(tour.child_amount):
                if not edit:
                    text = _l(message.from_user.id, 'tour9_0')
                    b_text = [_l(message.from_user.id, f'tour9_{i}') for i in [1, 2]]
                    keyboard = self.Keyboards.hotel_class(b_text, user)
                    return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
                else:
                    self.tour_filter_last_msg(message, tour, user)
            else:
                if counter == '1':
                    text = _l(message.from_user.id, 'tour8_1').format('второго')
                    tour.state = 'child_counter|2'
                elif counter == '2':
                    text = _l(message.from_user.id, 'tour8_1').format('третьего')
                    tour.state = 'child_counter|3'
                tour.save()
                keyboard = self.Keyboards.back_and_menu(user.chat_id)
                return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)
        else:
            text = _l(message.from_user.id, 'system2')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            return self.bot.send_message(message.from_user.id, text, reply_markup=keyboard)

    def tour_set_hotel_class(self, call, tour, user, edit=None):
        """set hotel class"""
        if call.data == 'hotel_class|NEXT':
            if tour.star2 and tour.star3:
                text_rate = '7:3'
            elif tour.star2 and tour.star4:
                text_rate = '7:4'
            elif tour.star2 and tour.star5:
                text_rate = '7:78'

            elif tour.star3 and tour.star4:
                text_rate = '3:4'
            elif tour.star3 and tour.star5:
                text_rate = '3:78'
            elif tour.star4 and tour.star5:
                text_rate = '4:78'
            elif tour.star2:
                text_rate = '7'  # it's 2 star ID !!!
            elif tour.star3:
                text_rate = '3'
            elif tour.star4:
                text_rate = '4'
            elif tour.star5:
                text_rate = '78'  # it's 5 star ID !!!
            tour.hotel_rating = text_rate
        logger.info(f'{tour.hotel_rating=}')

        tour.state = 'hotel_rating'
        tour.save()
        if not edit:
            text = _l(call.from_user.id, 'tour10_0')
            b_text = [_l(call.from_user.id, f'tour10_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.hotel(b_text, user_id=user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_switch_hotel_class(self, call, tour, user, edit=None):
        """method switch buttons"""
        _, hotel_class, switch = call.data.split('|')
        logger.info(f'{hotel_class=}')
        logger.info(f'{switch=}')

        if switch == 'True' and (
                (tour.star3 and tour.star4) or (tour.star3 and tour.star5)
                or (tour.star4 and tour.star5) or (tour.star2 and tour.star3)
                or (tour.star2 and tour.star4) or (tour.star2 and tour.star5)):
            return

        setattr(tour, hotel_class, switch)
        tour.save()
        b_text = [_l(call.from_user.id, f'tour9_{i}') for i in [1, 2]]
        keyboard = self.Keyboards.hotel_class(b_text, user)
        return self.bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.message_id,
                                                  reply_markup=keyboard)

    def tour_set_hotel_name(self, message_call, tour, user, edit=None):
        """method set hotel"""
        try:
            _, hotel_id, hotel_name = message_call.text.split('|')
            tour.hotel = {'id': hotel_id, 'name': hotel_name}  # place dict to DB
        except:
            tour.hotel = {}
        tour.state = 'hotel'
        tour.save()
        if not edit:
            text = _l(message_call.from_user.id, 'tour11_0')
            b_text = [_l(message_call.from_user.id, f'tour11_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.meal(b_text, user)
            return self.bot.send_message(message_call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(message_call, tour, user)

    def tour_set_meal(self, call, tour, user, edit=None):
        """set hotel meal"""
        if call.data == 'meal_ALL':
            tour.meal_type = ''
        elif call.data == 'meal_NEXT':
            meal = ':'.join([i for i, j in TourFilter.MEAL if getattr(tour, f"{j}") is True])  # join all picked types
            logger.info(f'{meal=}')
            tour.meal_type = meal
        tour.state = 'meal_type'
        tour.save()
        if not edit:
            text = _l(call.from_user.id, 'tour12_0')
            keyboard = self.Keyboards.currency(user_id=user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            self.tour_filter_last_msg(call, tour, user)

    def tour_switch_meal(self, call, tour, user):
        """method switch buttons"""
        _, meal_type, switch = call.data.split('|')

        setattr(tour, meal_type, switch)
        tour.save()

        b_text = [_l(call.from_user.id, f'tour11_{i}') for i in [1, 2]]
        keyboard = self.Keyboards.meal(b_text, user)
        return self.bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.message_id,
                                                  reply_markup=keyboard)

    def tour_set_currency(self, call, tour, user):
        """method set currency and generate final view with human readable str"""
        _, currency = call.data.split('|')
        tour.currency = currency
        tour.save()
        return self.tour_filter_last_msg(call, tour, user)

    def tour_filter_last_msg(self, call, tour, user):
        """method that send last msg in filter add flow and after edit"""
        tour.state = 'done'
        tour.save()

        ts = TourDetail.objects.filter(filters=tour)
        logger.info(ts)
        for t in ts:
            t.filters.remove(tour)
            t.save()
        ts1 = TourDetail.objects.filter(filters=tour)
        logger.info(ts1)
        user.state = 'regular'
        user.save()
        text = _l(call.from_user.id, 'tour13_0').format(
            tour.get_from_city_display(), tour.country['name'], tour.str_to_city(), tour.date_from, tour.date_till,
            tour.nights_from, tour.nights_to, tour.adult_amount, len(tour.child_amount),
            tour.str_child_ages(), tour.str_to_hotel(), tour.str_hotel_stars(), tour.str_meal_type(),
            tour.get_currency_display()
        )
        b_text = [_l(call.from_user.id, f'menu0_{i}') for i in [3, 4]]
        keyboard = self.Keyboards.end_filter_menu(*b_text, pk=tour.pk, user_id=user.chat_id)
        return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    ####################################################################### EDIT FLOW
    def menu_edit_filter(self, call, tour, user):
        """method generate all filter step buttons when user edit filter"""
        user.state = 'edit_menu'
        user.save()

        text = _l(call.from_user.id, 'edit0')
        b_text = [(_l(call.from_user.id, f'edit{i}'), c_data) for i, c_data in zip(range(1, 15), self.CALL_DATA)]
        keyboard = self.Keyboards.edit_filter(b_text, user_id=user.chat_id)
        return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    def menu_edit_send_to_method(self, call, tour, user, backward=None):
        """method catch call_data and switch menu to edit one step"""
        if backward:
            status = tour.state
        else:
            status = call.data
        if status == 'from_city':
            tour.state = 'start'
            self.tour_set_new(call, user, edit=True)
        elif status == 'country':
            tour.state = 'from_city'
            i_text = _l(call.from_user.id, 'tour1_0')
            i_b_text = _l(call.from_user.id, 'tour1_1')
            i_keyboard = self.Keyboards.country(i_b_text, user_id=user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=i_text, message_id=call.message.message_id,
                                       reply_markup=i_keyboard)
        elif status == 'to_city':
            tour.state = 'country'
            text = _l(call.from_user.id, 'tour2_0')
            b_text = [_l(call.from_user.id, f'tour2_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.to_city(b_text, user_id=user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)

        elif status == 'date_from':
            tour.state = 'to_city'
            text = _l(call.from_user.id, 'tour3')
            calendar, step = WMonthTelegramCalendar(locale='ru',
                                                    min_date=timezone.localtime(timezone.now()).date()
                                                             + relativedelta(days=+1)).build()

            keyboard = self.add_menu_to_calendar(user, calendar)       # MENU buttons add to calendar

            self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

        elif status == 'date_till':
            tour.state = 'date_from'
            text = _l(call.from_user.id, 'tour4')
            min_date = tour.date_from
            max_date = tour.date_from + relativedelta(days=+12)  # not bigger that +12 days from date_from
            calendar, step = WMonthTelegramCalendar(locale='ru', min_date=min_date, max_date=max_date).build()

            keyboard = self.add_menu_to_calendar(user, calendar)       # MENU buttons add to calendar

            self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

        elif status == 'nights_from':
            tour.state = 'date_till'
            text = _l(call.from_user.id, 'tour5')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status == 'nights_to':
            tour.state = 'nights_from'
            text = _l(call.from_user.id, 'tour6')
            keyboard = self.Keyboards.back_and_menu(user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)

        elif status in ['adults', 'adult_amount']:
            tour.state = 'nights_to'
            text = _l(call.from_user.id, 'tour7')
            keyboard = self.Keyboards.adults(user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status in ['childrens', 'child_amount', 'child_counter|1', 'child_counter|2', 'child_counter|3']:
            tour.state = 'adult_amount'
            text = _l(call.from_user.id, 'tour8_0')
            keyboard = self.Keyboards.childrens(user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status in ['hotel_class', 'hotel_rating']:
            tour.state = 'child_amount'
            text = _l(call.from_user.id, 'tour9_0')
            b_text = [_l(call.from_user.id, f'tour9_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.hotel_class(b_text, user)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status in ['hotel_name', 'hotel']:
            tour.state = 'hotel_rating'
            text = _l(call.from_user.id, 'tour10_0')
            b_text = [_l(call.from_user.id, f'tour10_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.hotel(b_text, user_id=user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status in ['meal', 'meal_type']:
            tour.state = 'hotel'
            text = _l(call.from_user.id, 'tour11_0')
            b_text = [_l(call.from_user.id, f'tour11_{i}') for i in [1, 2]]
            keyboard = self.Keyboards.meal(b_text, user)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        elif status in ['currency', 'done']:
            tour.state = 'meal_type'
            text = _l(call.from_user.id, 'tour12_0')
            keyboard = self.Keyboards.currency(user_id=user.chat_id)
            self.bot.edit_message_text(chat_id=call.from_user.id, text=text, message_id=call.message.message_id,
                                       reply_markup=keyboard)
        tour.save()
        if not backward:
            user.state = 'edit_step'
        return user.save()

    #######################################################################

    ####################################################################### SEARCH FLOW

    def search_tour_from_new_filter(self, call):
        """method try to find new tours from filter search"""
        _, tour_pk = call.data.split('|')
        logger.info(tour_pk)
        # text = _l(call.from_user.id, 'system0')
        # message = self.bot.send_message(call.from_user.id, text)

        tour_filter = TourFilter.objects.get(pk=tour_pk)
        result = new_search(tour_filter)
        if result:
            self.generate_search_result(call.from_user.id, tour_filter.pk)

    def generate_search_result(self, user_id, tour_filter_pk=None, p_number=1):
        """method generate search result"""
        tour_filter = TourFilter.objects.get(pk=tour_filter_pk)

        len_result = TourDetail.objects.filter(filters=tour_filter).count()
        logger.info(len_result)
        if len_result == 0:
            logger.info(11)
            text = _l(user_id, 'system1')
            return self.bot.send_message(user_id, text)
        else:
            logger.info(22)
            search_result = TourDetail.objects.filter(filters=tour_filter)
            self.universal_pagination(user_id, search_result, p_number, tour_filter.pk)

    def universal_pagination(self, user_id, search_result, p_number, tour_filter_pk):
        """universal method that generate 3 type of keyboards"""

        b_text = [_l(user_id, f'search{i}') for i in range(0, 3)]
        paginator = Paginator(search_result, 5)
        tours = paginator.get_page(p_number)

        for index, tour in enumerate(tours):
            tour_callback = TourCallback.objects.filter(tour=tour).last()
            text = make_text(tour_callback)

            if index + 1 == len(tours) and tours.has_next():  # ALL 3 buttons
                next_page_num = tours.next_page_number()
                keyboard = self.Keyboards.search_keyboard(*b_text, tour_key=tour.pk, page=next_page_num,
                                                          filter_pk=tour_filter_pk, number=2)

                self.bot.send_message(user_id, text, reply_markup=keyboard)
            elif index + 1 == len(tours) and not tours.has_next():  # 2 buttons

                keyboard = self.Keyboards.search_keyboard(*b_text, tour_key=tour.pk, filter_pk=tour_filter_pk, number=3)
                self.bot.send_message(user_id, text, reply_markup=keyboard)
            else:
                keyboard = self.Keyboards.search_keyboard(*b_text, tour_key=tour.pk, filter_pk=tour_filter_pk, number=1)
                self.bot.send_message(user_id, text, reply_markup=keyboard)

    #######################################################################

    ######################################################################## MONITORING FLOW

    def take_tour_to_monitoring(self, call, user):
        """method take tour key and start to monitoring it"""
        _, filter_pk, tour_key = call.data.split('|')
        logger.info(tour_key)
        logger.info(filter_pk)
        tour_detail = TourDetail.objects.get(key=tour_key)

        if not user.on_monitoring:
            filter_ = TourFilter.objects.get(pk=filter_pk)
            user.on_monitoring.append(filter_pk)  # TODO replace
            user.save()

            tour_callback = TourCallback.objects.filter(tour=tour_detail).last()

            TourFilter.objects.filter(pk=filter_pk).update(to_city=tour_callback.region,
                                                           date_from=tour_callback.date_on,
                                                           date_till=tour_callback.date_on,
                                                           nights_from=tour_callback.duration_nights,
                                                           nights_to=tour_callback.duration_nights,
                                                           hotel=tour_callback.hotel_dict
                                                           )

            text = _l(call.from_user.id, 'monitor0')
            monitoring_text = make_text(tour_callback)
            text += monitoring_text
            logger.info('done')
            MonitoringText.objects.create(user=user, filter=filter_,
                                          text=monitoring_text, callback=tour_callback)

            keyboard = self.Keyboards.new_monitoring_flow(filter_.pk, user_id=user.chat_id)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            last_filter = user.on_monitoring[-1]
            filter__ = TourFilter.objects.get(pk=last_filter)

            text = _l(call.from_user.id, 'monitor1')
            monitoring_text = MonitoringText.objects.filter(user=user, filter=filter__).last()
            text += monitoring_text.text
            keyboard = self.Keyboards.change_monitoring_flow(call, tour_key, filter_pk)

            text_splitter(self, user.chat_id, text, keyboard)

            # return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    def stop_tour_to_monitoring(self, call, user):
        """method take tour key and stop monitoring it"""
        _, filter_pk = call.data.split('|')
        logger.info(filter_pk)
        filter_ = TourFilter.objects.get(pk=filter_pk)

        monitoring_text = MonitoringText.objects.filter(user=user, filter=filter_)
        monitoring_text.delete()

        self.delete_msg(call.from_user.id, call.message.message_id)

        user.on_monitoring.remove(filter_pk)
        user.save()
        text = _l(call.from_user.id, 'monitor3')
        keyboard = self.Keyboards.make_new_filter(call)
        return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    def delete_msg(self, user_id, message_id):
        """method delete last msg"""
        try:
            self.bot.delete_message(chat_id=user_id, message_id=message_id)
        except:
            pass

    def stop_and_run(self, call, user):
        """method that stop old monitoring and activate new one"""
        _, new_tour_key, filter_pk = call.data.split('|')
        logger.info(new_tour_key)
        user.on_monitoring = []
        user.save()
        filter__ = TourFilter.objects.get(pk=filter_pk)
        tour_detail = TourDetail.objects.get(key=new_tour_key)
        tour_callback = TourCallback.objects.filter(tour=tour_detail).last()
        TourFilter.objects.filter(pk=filter_pk).update(to_city=tour_callback.region,
                                                         date_from=tour_callback.date_on,
                                                         date_till=tour_callback.date_on,
                                                         nights_from=tour_callback.duration_nights,
                                                         nights_to=tour_callback.duration_nights,
                                                         hotel=tour_callback.hotel_dict
                                                         )

        self.delete_msg(call.from_user.id, call.message.message_id)

        monitoring_text = make_text(tour_callback)
        logger.info(user)
        logger.info(filter__)
        logger.info(monitoring_text)
        MonitoringText.objects.create(user=user, filter=filter__, text=monitoring_text, callback=tour_callback)
        user.on_monitoring.append(filter_pk)
        user.save()
        text = _l(call.from_user.id, 'monitor0')
        text += monitoring_text
        keyboard = self.Keyboards.new_monitoring_flow(filter_pk, user_id=user.chat_id)
        return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)

    def generate_follow_view(self, call, user):
        """method generate monitoring view"""
        if not user.on_monitoring:
            text = _l(call.from_user.id, 'monitor6')
            keyboard = self.Keyboards.make_new_filter(call)
            return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
        else:
            try:
                last_filter = user.on_monitoring[-1]
                filter__ = TourFilter.objects.get(pk=last_filter)
                monitoring_text = MonitoringText.objects.filter(user=user, filter=filter__).last()

                text = _l(call.from_user.id, 'monitor5')
                text += monitoring_text.text
                keyboard = self.Keyboards.new_monitoring_flow(user.on_monitoring[-1], user_id=user.chat_id)

                text_splitter(self, user.chat_id, text, keyboard)

                # return self.bot.send_message(call.from_user.id, text, reply_markup=keyboard)
            except:
                pass
    
    def send_promo_mailing(self, promo_id, users):
        """send promo to customers."""
        promo = Promo.objects.get(id=promo_id)
        image_exists = bool(promo.image)
        
        for user in users:
            keyboard = self.Keyboards.menu_only_keyboard(user)
            try:
                if image_exists:
                    self.bot.send_photo(
                        user, promo.image, caption=f'<b>{promo.title}</b>\n\n{promo.text}', reply_markup=keyboard
                    )
                else:
                    self.bot.send_message(
                        user, f'<b>{promo.title}</b>\n\n{promo.text}', reply_markup=keyboard
                    )
                time.sleep(1)
            except ApiException:
                continue

    ########################################################################

    class Keyboards(Enum):

        def main_menu(user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2, = [_l(user_id, f'menu0_{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='MAKE_NEW_FILTER')
            button2 = types.InlineKeyboardButton(text2, callback_data='my_FOLLOW')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        def admin_buttons(b_text, chat_id):
            keyboard = types.InlineKeyboardMarkup(row_width=1)
            text1, text2 = b_text
            button1 = types.InlineKeyboardButton(text=text1, callback_data=f'accept|{chat_id}')
            button2 = types.InlineKeyboardButton(text=text2, callback_data=f'decline|{chat_id}')
            keyboard.row(button1, button2)
            return keyboard

        def tour_city(user_id):
            keyboard = types.InlineKeyboardMarkup()
            buttons = [types.InlineKeyboardButton(text=city_text, callback_data=f'city_from|{city_call}') for
                       city_call, city_text in TourFilter.CITY]
            keyboard.add(*buttons, row_width=2)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='backward')
            button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        # def adults(user_id):
        #     keyboard = types.InlineKeyboardMarkup()
        #     buttons = [types.InlineKeyboardButton(text=i, callback_data=f'adults|{i}') for i in range(1, 5)]
        #     keyboard.add(*buttons, row_width=4)
        #     text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
        #     button1 = types.InlineKeyboardButton(text1, callback_data='backward')
        #     button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
        #     keyboard.add(button1, button2, row_width=2)
        #     return keyboard

        def adults(user_id):
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text='1 💃', callback_data='adults|1')
            button2 = types.InlineKeyboardButton(text='2 👫', callback_data='adults|2')
            button3 = types.InlineKeyboardButton(text='3 🕺👫', callback_data='adults|3')
            button4 = types.InlineKeyboardButton(text='4 👫👫', callback_data='adults|4')
            keyboard.add(button1, button2, button3, button4, row_width=4)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='backward')
            button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        # def childrens(user_id):
        #     keyboard = types.InlineKeyboardMarkup()
        #     buttons = [types.InlineKeyboardButton(text=i, callback_data=f'childrens|{i}') for i in range(0, 4)]
        #     keyboard.add(*buttons, row_width=4)
        #     text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
        #     button1 = types.InlineKeyboardButton(text1, callback_data='backward')
        #     button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
        #     keyboard.add(button1, button2, row_width=2)
        #     return keyboard

        def childrens(user_id):
            keyboard = types.InlineKeyboardMarkup()
            button0 = types.InlineKeyboardButton(text='0', callback_data='childrens|0')
            button1 = types.InlineKeyboardButton(text='1 👶', callback_data='childrens|1')
            button2 = types.InlineKeyboardButton(text='2 👧🧒', callback_data='childrens|2')
            button3 = types.InlineKeyboardButton(text='3 👶👧🧒', callback_data='childrens|3')
            keyboard.add(button0, button1, button2, button3, row_width=4)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='backward')
            button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        def hotel_class(b_text, user):
            keyboard = types.InlineKeyboardMarkup()
            emo = f"{emoji.emojize(':white_check_mark:', use_aliases=True)}"
            tour = TourFilter.objects.filter(user=user).last()
            if tour.star2:
                button2 = types.InlineKeyboardButton(text=f'2 ⭐️⭐{emo}', callback_data=f'h_c|star2|False')
            else:
                button2 = types.InlineKeyboardButton(text='2 ⭐️⭐', callback_data=f'h_c|star2|True')
            if tour.star3:
                button3 = types.InlineKeyboardButton(text=f'3 ⭐️⭐️⭐️{emo}', callback_data=f'h_c|star3|False')
            else:
                button3 = types.InlineKeyboardButton(text='3 ⭐️⭐️⭐️', callback_data=f'h_c|star3|True')
            if tour.star4:
                button4 = types.InlineKeyboardButton(text=f'4 ⭐️⭐️⭐️⭐{emo}', callback_data=f'h_c|star4|False')
            else:
                button4 = types.InlineKeyboardButton(text='4 ⭐️⭐️⭐️⭐', callback_data=f'h_c|star4|True')
            if tour.star5:
                button5 = types.InlineKeyboardButton(text=f'5 ⭐️⭐️⭐️⭐️⭐{emo}', callback_data=f'h_c|star5|False')
            else:
                button5 = types.InlineKeyboardButton(text='5 ⭐️⭐️⭐️⭐️⭐', callback_data=f'h_c|star5|True')
            keyboard.add(button2, button3, button4, button5, row_width=2)

            text1, text2 = b_text

            if tour.star2 or tour.star3 or tour.star4 or tour.star5:
                button20 = types.InlineKeyboardButton(text=text2, callback_data='hotel_class|NEXT')
                keyboard.add(button20)
            text1, text2, = [_l(user.chat_id, f'menu{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='backward')
            button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        def meal(b_text, user):
            keyboard = types.InlineKeyboardMarkup()
            emo = f"{emoji.emojize(':white_check_mark:', use_aliases=True)}"
            tour = TourFilter.objects.filter(user=user).last()
            if tour.AI:
                button1 = types.InlineKeyboardButton(text=f'AI{emo}', callback_data=f'meal|AI|False')
            else:
                button1 = types.InlineKeyboardButton(text='AI', callback_data=f'meal|AI|True')
            if tour.BB:
                button2 = types.InlineKeyboardButton(text=f'BB{emo}', callback_data=f'meal|BB|False')
            else:
                button2 = types.InlineKeyboardButton(text='BB', callback_data=f'meal|BB|True')
            if tour.FB:
                button3 = types.InlineKeyboardButton(text=f'FB{emo}', callback_data=f'meal|FB|False')
            else:
                button3 = types.InlineKeyboardButton(text='FB', callback_data=f'meal|FB|True')
            if tour.HB:
                button4 = types.InlineKeyboardButton(text=f'HB{emo}', callback_data=f'meal|HB|False')
            else:
                button4 = types.InlineKeyboardButton(text='HB', callback_data=f'meal|HB|True')
            if tour.UAI:
                button5 = types.InlineKeyboardButton(text=f'UAI{emo}', callback_data=f'meal|UAI|False')
            else:
                button5 = types.InlineKeyboardButton(text='UAI', callback_data=f'meal|UAI|True')
            if tour.RO:
                button6 = types.InlineKeyboardButton(text=f'RO{emo}', callback_data=f'meal|RO|False')
            else:
                button6 = types.InlineKeyboardButton(text='RO', callback_data=f'meal|RO|True')
            keyboard.add(button1, button2, button3, button4, button5, button6, row_width=3)

            text1, text2 = b_text

            if tour.AI or tour.BB or tour.FB or tour.HB or tour.UAI or tour.RO:
                button20 = types.InlineKeyboardButton(text=text2, callback_data='meal_NEXT')
                keyboard.add(button20)
            else:
                button10 = types.InlineKeyboardButton(text=text1, callback_data='meal_ALL')
                keyboard.add(button10)
            text1, text2, = [_l(user.chat_id, f'menu{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='backward')
            button2 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button1, button2, row_width=2)
            return keyboard

        def country(b_text, user_id):
            keyboard = types.InlineKeyboardMarkup(row_width=1)
            button1 = types.InlineKeyboardButton(text=b_text, switch_inline_query_current_chat='country____')
            keyboard.row(button1)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def to_city(b_text, user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2 = b_text
            button1 = types.InlineKeyboardButton(text=text1, switch_inline_query_current_chat='to_city____')
            button2 = types.InlineKeyboardButton(text=text2, callback_data='ALL_to_city')
            keyboard.add(button1, button2, row_width=1)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def hotel(b_text, user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2 = b_text
            button1 = types.InlineKeyboardButton(text=text1, switch_inline_query_current_chat='hotel____')
            # button2 = types.InlineKeyboardButton(text=text2, callback_data='ALL_hotel')
            # keyboard.add(button1, button2, row_width=1)
            keyboard.add(button1, row_width=1)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def currency(user_id):
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text='UAH', callback_data='currency|2')
            button2 = types.InlineKeyboardButton(text='у.е. по курсу ТО', callback_data='currency|1')
            keyboard.add(button1, button2, row_width=2)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def end_filter_menu(*b_text, pk, user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2 = b_text
            button1 = types.InlineKeyboardButton(text=text1, callback_data=f'NEW_SEARCH|{pk}')
            button2 = types.InlineKeyboardButton(text=text2, callback_data='edit_filter')
            keyboard.add(button1, button2, row_width=1)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button66, row_width=1)
            return keyboard

        def edit_filter(b_text, user_id):
            keyboard = types.InlineKeyboardMarkup()
            buttons = [types.InlineKeyboardButton(text, callback_data=f'{call_data}') for text, call_data in b_text]
            keyboard.add(*buttons, row_width=2)
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def back_and_menu(user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2, = [_l(user_id, f'menu{i}') for i in [1, 2]]
            button55 = types.InlineKeyboardButton(text1, callback_data='backward')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button55, button66, row_width=2)
            return keyboard

        def search_keyboard(*b_text, tour_key=None, page=None, filter_pk=None, number=1):
            b_t1, b_t2, b_t3 = b_text
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(b_t1, callback_data=f'MONITORING|{filter_pk}|{tour_key}')
            button2 = types.InlineKeyboardButton(b_t2, callback_data='MAKE_NEW_FILTER')
            button3 = types.InlineKeyboardButton(b_t3, callback_data=f'show_more|{filter_pk}|{page}')
            if number == 1:
                keyboard.row(button1)
                return keyboard
            elif number == 2:
                keyboard.row(button1)
                keyboard.row(button2, button3)
                return keyboard
            else:
                keyboard.row(button1)
                keyboard.row(button2)
                return keyboard

        def new_monitoring_flow(filter_pk, user_id):
            keyboard = types.InlineKeyboardMarkup()
            b_t1, b_t2 = [_l(user_id, f'search{i}') for i in [3, 1]]
            button1 = types.InlineKeyboardButton(text=b_t1, callback_data=f'STOP_monit|{filter_pk}')
            button2 = types.InlineKeyboardButton(text=b_t2, callback_data='MAKE_NEW_FILTER')
            keyboard.add(button1, button2, row_width=1)
            text2 = _l(user_id, 'menu2')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button66, row_width=1)
            return keyboard

        def menu_only_keyboard(user_id):
            keyboard = types.InlineKeyboardMarkup()
            text2 = _l(user_id, 'menu2')
            button66 = types.InlineKeyboardButton(text2, callback_data='MENU')
            keyboard.add(button66, row_width=1)
            return keyboard

        def change_monitoring_flow(call, tour_key, filter_pk):
            keyboard = types.InlineKeyboardMarkup()
            b_t1, b_t2, b_t3, b_t4 = [_l(call.from_user.id, f'search{i}') for i in [4, 5, 6, 1]]
            button1 = types.InlineKeyboardButton(text=b_t1, callback_data=f'my_FOLLOW')
            button2 = types.InlineKeyboardButton(text=b_t2, callback_data=f'SAR|{tour_key}|{filter_pk}')
            button3 = types.InlineKeyboardButton(text=b_t3, callback_data='backward_to_monitoring')
            button4 = types.InlineKeyboardButton(text=b_t4, callback_data='MAKE_NEW_FILTER')
            keyboard.add(button1, button2, row_width=1)
            keyboard.add(button3, button4, row_width=2)
            return keyboard

        def make_new_filter(call):
            keyboard = types.InlineKeyboardMarkup()
            b_t1 = _l(call.from_user.id, 'search1')
            button1 = types.InlineKeyboardButton(text=b_t1, callback_data='MAKE_NEW_FILTER')
            keyboard.add(button1)
            return keyboard

