import os
import telebot

from django.conf import settings
from django.db import models


class BotConfig(models.Model):
    token = models.CharField(max_length=100, blank=True, default='')
    is_active = models.BooleanField(default=False)
    server_url = models.CharField(max_length=200, blank=True, default='')

    def get_me(self):
        tbot = telebot.TeleBot(self.token)
        bot_name = tbot.get_me()
        return bot_name.username

    def set_hook(self):
        bot = telebot.TeleBot(self.token)
        webhook_url = self.server_url + '/get_hook/'

        # ssl_cert = os.path.join(settings.BASE_DIR, 'docker', 'nginx', 'nginx-selfsigned.pem')  # Путь к сертификату
        # bot.set_webhook(webhook_url, certificate=open(ssl_cert, 'r'))
        bot.set_webhook(webhook_url)

    def save(self, *args, **kwargs):
        self.set_hook()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.get_me()


class ApiToken(models.Model):
    api_token = models.CharField(max_length=100, blank=True, default='')
    active = models.BooleanField(default=False)
