import os
import telebot
import i18n

from .models import BotConfig, ApiToken
from administrator.models import User, TourFilter
from telebot import util


def bot_update(json_data):
    return telebot.types.Update.de_json(json_data)


def get_token():
    config = ApiToken.objects.filter(active=True)
    if config:
        return config.last().api_token
    else:
        return os.getenv("API_KEY")


def get_lang_text(user_id, var):
    """метод, який вертає текстове представлення кнопки чи тексту у відповідності до мови юзера"""
    try:
        customer = User.objects.get(chat_id=user_id)
        i18n.set('locale', f'{customer.language_code}')
    except:
        i18n.set('locale', f'ru')
    return i18n.t(f'translate.{var}')


def check_user_role(chat_id):
    """визначення ролі юзера і повертання її"""
    user = User.objects.filter(chat_id=chat_id)
    if user:
        return user.last().role


def check_user_state(chat_id):
    """визначення стейту юзера і повертання його в мейн"""
    user = User.objects.filter(chat_id=chat_id)
    if user:
        return user.first().state


def get_user(chat_id):
    """get user from db and return it"""
    user = User.objects.filter(chat_id=chat_id)
    if user:
        return user.last()


def get_tour(chat_id):
    """get tour from db and return it"""
    user = User.objects.filter(chat_id=chat_id).last()
    if user:
        tour = TourFilter.objects.filter(user=user).last()
        if tour:
            return tour


def text_splitter(Bot, chat_id, text, keyboard):
    """split huge text ... or not if it small"""
    splitted_text = util.split_string(text, 3000)
    # logger.info(len(f'{splitted_text=}'))
    for index, text in enumerate(splitted_text, start=1):
        if index == len(splitted_text):
            return Bot.bot.send_message(chat_id, text, reply_markup=keyboard)
        else:
            Bot.bot.send_message(chat_id, text)
