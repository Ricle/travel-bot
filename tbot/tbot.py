import os
import telebot
import i18n

from django.conf import settings
from .models import BotConfig
from django.db import connection


class tBot:

    def __init__(self):
        if 'tbot_botconfig' in connection.introspection.table_names() and BotConfig.objects.filter(is_active=True):
            config = BotConfig.objects.get(is_active=True)
            self.bot = telebot.TeleBot(config.token,  parse_mode='HTML')
        else:
            self.bot = telebot.TeleBot(os.getenv('TOKEN'), parse_mode='HTML')
        i18n.load_path.append(os.path.join(settings.BASE_DIR, 'translate'))


