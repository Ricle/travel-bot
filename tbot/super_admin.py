from enum import Enum
from telebot import types

from .tbot import tBot
from administrator.models import User
from .bot_tools import get_lang_text as _l


class AdministratorBot(tBot):
    """main methods class"""

    def action_to_user(self, call):
        """method that accept or decline user registration"""
        action, chat_id = call.data.split('|')
        user = User.objects.get(chat_id=chat_id)
        if action == 'accept':
            user.accept = True
            user.state = 'regular'
            self.user_main_menu(user.chat_id)
        elif action == 'decline':
            user.accept = False
            user.state = 'start'
            text = _l(user.chat_id, 'reg3')
            self.bot.send_message(user.chat_id, text, reply_markup=types.ReplyKeyboardRemove())
        self.bot.edit_message_reply_markup(chat_id=call.from_user.id, message_id=call.message.message_id,
                                           reply_markup=None)
        user.save()

    def user_main_menu(self, chat_id):
        """method send main menu to user"""
        text = _l(chat_id, 'menu0_0')
        keyboard = self.Keyboards.main_menu(chat_id)
        return self.bot.send_message(chat_id, text, reply_markup=keyboard)

    class Keyboards(Enum):

        def main_menu(user_id):
            keyboard = types.InlineKeyboardMarkup()
            text1, text2, = [_l(user_id, f'menu0_{i}') for i in [1, 2]]
            button1 = types.InlineKeyboardButton(text1, callback_data='MAKE_NEW_FILTER')
            button2 = types.InlineKeyboardButton(text2, callback_data='my_FOLLOW')
            keyboard.add(button1, button2, row_width=2)
            return keyboard



