from django.contrib import admin
from .models import BotConfig, ApiToken

admin.site.register(BotConfig)
admin.site.register(ApiToken)
