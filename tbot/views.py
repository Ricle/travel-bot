from loguru import logger
from dateutil.relativedelta import relativedelta
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP

from .bot_tools import *
from .api_callback import *
from .tbot import tBot
from .super_admin import AdministratorBot
from .customer import CustomerBot
from .bot_tools import get_lang_text as _l
from administrator.tasks import generate_tours

TBot = tBot()


@csrf_exempt
def get_hook(request):
    if request.META['CONTENT_TYPE'] == 'application/json':
        json_data = request.body.decode('utf-8')
        update = bot_update(json_data)
        # logger.info('update:', update)
        TBot.bot.process_new_updates([update])
        return HttpResponse(status=200)
    else:
        raise PermissionDenied


# хендлер для календаря
@TBot.bot.callback_query_handler(func=DetailedTelegramCalendar.func())
def cal(call):
    CBot = CustomerBot()
    user = get_user(call.from_user.id)
    tour = get_tour(call.from_user.id)

    if tour.state == 'to_city':
        text = _l(call.from_user.id, 'tour3')
        min_date = timezone.localtime(timezone.now()).date() + relativedelta(days=+1)  # not early tomorrow
        max_date = None
    elif tour.state == 'date_from':
        text = _l(call.from_user.id, 'tour4')
        min_date = tour.date_from
        max_date = tour.date_from + relativedelta(days=+12)  # not bigger that +12 days from date_from

    result, key, step = DetailedTelegramCalendar(locale='ru', min_date=min_date, max_date=max_date).process(call.data)
    if not result and key:
        keyboard = CBot.add_menu_to_calendar(user, key)  # MENU buttons add to calendar
        TBot.bot.edit_message_text(text,
                                   call.message.chat.id,
                                   call.message.message_id,
                                   reply_markup=keyboard)

    elif result:
        logger.info(result)
        if user.state == 'tour_filter':
            if tour.state == 'to_city':
                CBot.tour_set_date_from(call, tour, result, user)
            elif tour.state == 'date_from':
                CBot.tour_set_date_till(call, tour, result, user)
        elif user.state == 'edit_step':
            if tour.state == 'to_city':
                CBot.tour_set_date_from(call, tour, result, user, edit=True)
            elif tour.state == 'date_from':
                CBot.tour_set_date_till(call, tour, result, user, edit=True)


@TBot.bot.message_handler(commands=['start'])
def start(message):
    """хендлер, що опрацьовує натискання кнопки start як для нових так і для існуючих юзерів"""

    CBot = CustomerBot()
    user = get_user(message.from_user.id)

    if not user or not user.accept:
        CBot.user_new_make(message)

    elif user and user.role == 'Customer' and user.accept:
        CBot.user_main_menu(message)

    elif user and user.role == 'Administrator':
        pass


# handler that take message from CUSTOMER
@TBot.bot.message_handler(func=lambda message: True and check_user_role(message.from_user.id) == 'Customer')
def rew55(message):
    logger.info(message.text)
    CBot = CustomerBot()
    user = get_user(message.from_user.id)
    tour = get_tour(message.from_user.id)

    if message.text == _l(user.chat_id, 'menu2'):
        CBot.user_main_menu(message)

    elif user.state == 'start':
        CBot.user_set_name(message, user)

    elif user.state == 'name':
        CBot.user_set_phone(message, user)

    elif user.state == 'phone':
        CBot.user_set_agency(message, user)
    ##########################################################################
    elif user.state == 'regular' and message.text == _l(user.chat_id, 'menu0_1'):
        logger.info('NEW')
        CBot.tour_set_new(message, user)

    elif user.state == 'regular' and message.text == _l(user.chat_id, 'menu0_2'):
        CBot.generate_follow_view(message, user)

    elif user.state == 'tour_filter' and message.text == _l(user.chat_id, 'menu1'):
        logger.info('BACKWARD')
        CBot.menu_edit_send_to_method(message, tour, user, backward=True)

    elif user.state == 'tour_filter':
        if 'choose_country|' in message.text:
            CBot.tour_set_country(message, tour, user)

        elif 'choose_to_city|' in message.text:
            CBot.tour_set_to_city(message, tour, user)

        elif message.text and tour.state == 'date_till':
            CBot.tour_set_nights_from(message, tour, user)

        elif message.text and tour.state == 'nights_from':
            CBot.tour_set_nights_to(message, tour, user)

        elif message.text and 'child_counter|' in tour.state:
            CBot.tour_set_child_age(message, tour, user)

        elif 'choose_hotels|' in message.text:
            CBot.tour_set_hotel_name(message, tour, user)

############################################
    elif user.state == 'edit_step':
        if 'choose_country|' in message.text:
            CBot.tour_set_country(message, tour, user, edit=True)
        elif 'choose_to_city|' in message.text:
            CBot.tour_set_to_city(message, tour, user, edit=True)
        elif 'choose_hotels|' in message.text:
            CBot.tour_set_hotel_name(message, tour, user, edit=True)
        elif message.text and tour.state == 'date_till':
            CBot.tour_set_nights_from(message, tour, user, edit=True)
        elif message.text and tour.state == 'nights_from':
            CBot.tour_set_nights_to(message, tour, user, edit=True)
        elif message.text and 'child_counter|' in tour.state:
            CBot.tour_set_child_age(message, tour, user, edit=True)
        elif 'choose_hotels|' in message.text:
            CBot.tour_set_hotel_name(message, tour, user, edit=True)


@TBot.bot.callback_query_handler(func=lambda call: True and check_user_role(call.from_user.id) == 'Customer')
def rew6335(call):
    logger.info(call.data)
    CBot = CustomerBot()
    user = get_user(call.from_user.id)
    tour = get_tour(call.from_user.id)

    if call.data == 'MENU':
        CBot.user_main_menu(call)

    elif call.data == 'backward':
        if user.state == 'edit_menu':
            CBot.tour_filter_last_msg(call, tour, user)
        elif user.state == 'edit_step':
            CBot.menu_edit_filter(call, tour, user)
        elif tour.state == 'start':
            CBot.user_main_menu(call, back=True)
        else:
            CBot.menu_edit_send_to_method(call, tour, user, backward=True)

    elif user.state == 'tour_filter':
        if 'city_from|' in call.data:
            CBot.tour_set_from_city(call, tour, user)
        elif call.data == 'ALL_to_city':
            CBot.tour_set_to_city(call, tour, user)

        elif 'adults|' in call.data:
            CBot.tour_set_adults(call, tour, user)
        elif 'childrens|' in call.data:
            CBot.tour_set_childrens(call, tour, user)
        elif call.data == 'hotel_class|NEXT':
            CBot.tour_set_hotel_class(call, tour, user)
        elif 'h_c|star' in call.data:
            CBot.tour_switch_hotel_class(call, tour, user)

        elif call.data == 'ALL_hotel':
            CBot.tour_set_hotel_name(call, tour, user)

        elif call.data in ['meal_ALL', 'meal_NEXT']:
            CBot.tour_set_meal(call, tour, user)

        elif 'meal|' in call.data:
            CBot.tour_switch_meal(call, tour, user)

        elif call.data in ['currency|1', 'currency|2']:
            CBot.tour_set_currency(call, tour, user)

    elif user.state == 'regular':
        if call.data == 'edit_filter':
            CBot.menu_edit_filter(call, tour, user)

        elif 'NEW_SEARCH|' in call.data:
            _, tour_pk = call.data.split('|')
            generate_tours.delay(tour_pk, call.from_user.id)  # CELERY TASK
            # CBot.search_tour_from_new_filter(call)

        elif 'show_more|' in call.data:
            _, filter_pk, page = call.data.split('|')
            CBot.generate_search_result(call.from_user.id, filter_pk, p_number=page)

        elif 'MONITORING|' in call.data:
            CBot.take_tour_to_monitoring(call, user)

        elif 'STOP_monit|' in call.data:
            CBot.stop_tour_to_monitoring(call, user)

        elif call.data == 'MAKE_NEW_FILTER':
            CBot.tour_set_new(call, user)

        elif call.data == 'backward_to_monitoring':
            CBot.delete_msg(call.from_user.id, call.message.message_id)

        elif 'SAR|' in call.data:
            CBot.stop_and_run(call, user)

        elif call.data == 'my_FOLLOW':
            CBot.generate_follow_view(call, user)


#######################################################
    elif user.state == 'edit_menu':
        if call.data in CBot.CALL_DATA:
            CBot.menu_edit_send_to_method(call, tour, user)

    elif user.state == 'edit_step':
        if 'city_from|' in call.data:
            CBot.tour_set_from_city(call, tour, user, edit=True)
        elif call.data == 'ALL_to_city':
            CBot.tour_set_to_city(call, tour, user, edit=True)
        elif 'adults|' in call.data:
            CBot.tour_set_adults(call, tour, user, edit=True)
        elif 'childrens|' in call.data:
            CBot.tour_set_childrens(call, tour, user, edit=True)
        elif call.data == 'hotel_class|NEXT':
            CBot.tour_set_hotel_class(call, tour, user, edit=True)
        elif 'h_c|star' in call.data:
            CBot.tour_switch_hotel_class(call, tour, user)
        elif call.data == 'ALL_hotel':
            CBot.tour_set_hotel_name(call, tour, user, edit=True)
        elif call.data in ['meal_ALL', 'meal_NEXT']:
            CBot.tour_set_meal(call, tour, user, edit=True)
        elif 'meal|' in call.data:
            CBot.tour_switch_meal(call, tour, user)
        elif call.data in ['currency|1', 'currency|2']:
            CBot.tour_set_currency(call, tour, user)


@TBot.bot.inline_handler(func=lambda query: True and check_user_role(query.from_user.id) == 'Customer')
def rew988(inline_query):
    user = get_user(inline_query.from_user.id)
    tour = get_tour(inline_query.from_user.id)

    logger.info(inline_query.query)
    if "country____" in inline_query.query:
        _, text = inline_query.query.split('____')

        if len(text) >= 3:
            TBot.bot.answer_inline_query(
                inline_query_id=inline_query.id,
                results=gen_country_list(text),
                cache_time=0
            )

    elif "to_city____" in inline_query.query:
        _, text = inline_query.query.split('____')

        if len(text) >= 3:
            TBot.bot.answer_inline_query(
                inline_query_id=inline_query.id,
                results=gen_city_list(text, tour),
                cache_time=0
            )

    elif "hotel____" in inline_query.query:
        _, text = inline_query.query.split('____')

        if len(text) >= 3:
            TBot.bot.answer_inline_query(
                inline_query_id=inline_query.id,
                results=gen_hotel_list(text, tour),
                cache_time=0
            )


@TBot.bot.callback_query_handler(func=lambda call: True and check_user_role(call.from_user.id) == 'Administrator')
def rew63235(call):
    logger.info(call.data)
    ABot = AdministratorBot()

    if 'accept|' in call.data or 'decline|' in call.data:
        ABot.action_to_user(call)
