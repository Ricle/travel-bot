import requests

from loguru import logger
from telebot import types
from administrator.models import TourDetail, TourCallback
from .bot_tools import *
from .bot_tools import get_lang_text as _l
from .tbot import tBot

TBot = tBot()

# headers = {'Authorization': f'{os.getenv("API_KEY")}'}
headers = {'Authorization': f'{get_token()}'}


def country_list(text):
    """get country list from search query"""
    res = requests.get(
        url=f'https://api.ittour.com.ua/module/params/destinations?type=1&query={text}',
        headers=headers)
    return res.json()['countries']


def city_hotels_list(text, country_id, region_id=None, hotels=None):
    """get city list from search query"""
    url = f'https://api.ittour.com.ua/module/params/destinations?type=1&country={country_id}&query={text}'
    if region_id:
        url += f'&region={region_id}'
    res = requests.get(
        url=url,
        headers=headers)
    if not hotels:
        return res.json()['regions']
    else:
        return res.json()['hotels']


def gen_country_list(query):
    """method generate inline country list"""
    countrys = country_list(query)
    c = 0
    result = []
    for country in countrys:
        result.append(
            types.InlineQueryResultArticle(
                id=c,
                title=f"{country['name']}",
                input_message_content=types.InputTextMessageContent(
                    f"choose_country|{country['id']}|{country['name']}",
                ),
                description=f"Choose",
                thumb_height=1,
            )
        )
        c += 1
    return result


def gen_city_list(query, tour):
    """method generate inline city list"""
    city_list_ = city_hotels_list(query, tour.country['id'])
    c = 0
    result = []
    for citys in city_list_:
        result.append(
            types.InlineQueryResultArticle(
                id=c,
                title=f"{citys['name']}",
                input_message_content=types.InputTextMessageContent(
                    f"choose_to_city|{citys['id']}|{citys['name']}",
                ),
                description=f"Choose",
                thumb_height=1,
            )
        )
        c += 1
    return result


def gen_hotel_list(query, tour):
    """method generate inline hotel list"""
    if tour.to_city:
        hotel_list_ = city_hotels_list(query, tour.country['id'], tour.to_city['id'], hotels=True)
    else:
        hotel_list_ = city_hotels_list(query, tour.country['id'], hotels=True)

    ## SORT hotels in stars parameter ##
    star_ids = tour.hotel_rating.split(':')
    new_hotel_list = []
    for star_id in star_ids:
        for i in hotel_list_:
            if i['hotel_rating_id'] == int(star_id):
                new_hotel_list.append(i)
    ##
    c = 0
    result = []
    for hotel in new_hotel_list:
        result.append(
            types.InlineQueryResultArticle(
                id=c,
                title=f"{hotel['name']}",
                input_message_content=types.InputTextMessageContent(
                    f"choose_hotels|{hotel['id']}|{hotel['name']}",
                ),
                description=f"Choose",
                thumb_height=1,
            )
        )
        c += 1
    return result


def find_unique_offers(offers_list):
    """find unique offers in all version of search"""
    hotels_ = [offer['hotel_id'] for offer in offers_list]
    unique_offers = []
    for hotel_ in set(hotels_):
        hotel_offers = [offer for offer in offers_list if offer['hotel_id'] == hotel_]
        date_ = [offer['date_from'] for offer in hotel_offers]
        logger.info(set(date_))
        durations_ = [offer['duration'] for offer in hotel_offers]
        logger.info(set(durations_))
        b = sorted(hotel_offers, key=lambda x: (x['date_from'], x['duration'], x['prices']['2']))
        logger.info(b)
        for date in set(date_):
            for duration in set(durations_):
                for offer in b:
                    if offer['duration'] == duration and offer['date_from'] == date:
                        unique_offers.append(offer)
                        break

    # logger.info(unique_offers)
    return unique_offers


def precheck_tour(offers_list):
    """validate tour with stop_sale, stop_flight and price_change."""
    for offer in offers_list:
        res = requests.get(
            url=f'https://api.ittour.com.ua/tour/validate/{offer["key"]}',
            headers=headers
        )
        logger.info(result := res.json())
        try:
            if any([result['stop_sale'], result['stop_flight']]):
                logger.info('DELETED FROM LIST')
                offers_list.remove(offer)
            elif result['price_changed']:
                offer['prices'] = result['prices']

        except KeyError:
            offers_list.remove(offer)
    
    offers_list = sorted(offers_list, key=lambda offer: offer['prices']['2'])

    return offers_list


def gen_offers_list(tour_filter, unique=True):
    """generate all offers in case of filter"""
    offers_list = []
    page = 1
    while True:
        url = main_url(tour_filter, page)
        logger.info(url)
        res = requests.get(
            url=url,
            headers=headers)
        offers_list += (res.json()['offers'])
        if res.json()['has_more_pages']:
            page += 1
        else:
            break

    logger.info(page)
    logger.info(f'LEN {len(offers_list)}')
    ### NEW LOGIC
    pass_check = False
    valid_offer_keys = []

    if unique:
        while not pass_check:
            # logger.info(f'LEN OF OFFERS_LIST {len(offers_list)}')
            #### GET THE BEST OFFERS
            unique_offers = find_unique_offers(offers_list)
            for offer in unique_offers:
                logger.info(f"{offer['prices']['1']} - {offer['prices']['2']}")
            ###########################
            price_changed = False
            ###########################
            logger.info(f'LEN OF UNIQUE_OFFERS_LIST {len(unique_offers)}')
            #### VALIDATE THE BEST OFFERS
            not_valid_offers = []
            for offer in unique_offers:
                if offer["key"] not in valid_offer_keys:
                    res = requests.get(
                        url=f'https://api.ittour.com.ua/tour/validate/{offer["key"]}',
                        headers=headers
                    )
                    logger.info(offer)
                    logger.info(result := res.json())
                    try:
                        if any([result['stop_sale'], result['stop_flight']]):
                            logger.info('DELETED FROM LIST')
                            not_valid_offers.append(offer)

                        elif result['price_changed']:
                            if offer['prices'] == result['prices']:
                                logger.info('CHANGE PRICE VALID')
                                valid_offer_keys.append(offer["key"])
                            else:
                                offer['prices'] = result['prices']
                                price_changed = True
                                logger.info('CHANGE PRICE')

                        else:
                            logger.info('ADDED TO VALID')
                            valid_offer_keys.append(offer["key"])

                    except KeyError:
                        logger.info('DELETED FROM LIST')
                        not_valid_offers.append(offer)
                else:
                    logger.info('TOUR IS VALID - SKIP VALIDATION')
            #############################
            # logger.info(f'LEN OF NOT VALID {len(not_valid_offers)}')
            # CHECK IF THERE ARE INVALID OFFERS
            # If True remove them from original list and repeat
            if not_valid_offers:
                logger.info('REMOVE INVALID OFFERS')
                for offer in not_valid_offers:
                    offers_list.remove(offer)

            elif price_changed:
                logger.info('THERE ARE CHANGED PRICE')

            else:
                pass_check = True
                logger.info(sorted(unique_offers, key=lambda offer: offer['prices']['2']))
                return sorted(unique_offers, key=lambda offer: offer['prices']['2'])
                # return sorted(unique_offers, key=lambda x: (x['date_from'], x['prices']['2']))
    else:
        return offers_list


# def periodic_search(tour_filter):
#     """get tours list in every hour check"""
#     unique_offers = gen_offers_list(tour_filter)
#     if not unique_offers:
#         return None, False      # return it when offers list is null
#     else:
#         logger.info('unique periodik', len(unique_offers))
#         for offer in unique_offers:
#             tour_callback = detail_tour_info(offer['key'], tour_filter.user.chat_id)
#             tour_detail, _ = TourDetail.objects.get_or_create(key=offer['key'],
#                                                               tour_id=tour_callback['tour_id'])
#             tour_detail.order_int = offer['prices']['1']
#             tour_detail.users.add(tour_filter.user)
#             tour_detail.filters.add(tour_filter)
#             tour_detail.save()
#             tour_callback_, status = save_new_callback_to_db(tour_detail, tour_callback, tour_filter)
#             return tour_callback_, status   # return OBJ and status=True if there is new data OR status=False if not


def periodic_search(tour_filter):
    """get tours list in every hour check"""
    offers = gen_offers_list(tour_filter, unique=False)
    logger.info(offers)
    logger.info([offer['prices']['2'] for offer in offers])
    if not offers:
        return None, False      # return it when offers list is null

    else:
        logger.info(f'offers count {len(offers)}')
        for offer in offers:
            # # VALIDATE
            # res = requests.get(
            #     url=f'https://api.ittour.com.ua/tour/validate/{offer["key"]}',
            #     headers=headers
            # )
            # result = res.json()
            # if any([result['stop_sale'], result['stop_flight']]):
            #     return None, False
            # ###########
            tour_callback = detail_tour_info(offer['key'], tour_filter.user.chat_id) ### HERE
            if mega_check(tour_callback):   # MEGA check
                ## VALIDATION
                res = requests.get(
                    url=f'https://api.ittour.com.ua/tour/validate/{offer["key"]}',
                    headers=headers
                )
                logger.info(data := res.json())
                try:
                    if data['price_changed']:
                        tour_callback['prices'] = data['prices']
                except:
                    pass

                tour_detail, f_status = TourDetail.objects.get_or_create(key=offer['key'],
                                                                  tour_id=tour_callback['tour_id'])
                tour_detail.order_int = offer['prices']['1']
                tour_detail.users.add(tour_filter.user)
                tour_detail.filters.add(tour_filter)
                tour_detail.save()
                tour_callback_, status = save_new_callback_to_db(tour_detail, tour_callback, tour_filter)
                return tour_callback_, status   # return OBJ and status=True if there is new data OR status=False if not
            else:
                pass
        return None, False      # return it when offers list hasn't one with fly info


# def new_search(tour_filter):
#     """get tours list from API call"""
#
#     unique_offers = gen_offers_list(tour_filter)
#     # logger.info(unique_offers)
#     if not unique_offers:
#         return False
#     else:
#         for offer in unique_offers:
#             logger.info(offer['key'])
#             if has_flights_info(offer['key'], tour_filter.user.chat_id):      # if offers has flights info
#                 tour_callback = detail_tour_info(offer['key'], tour_filter.user.chat_id)
#             else:
#                 tour_callback = None    # in case where all offers withOUT flights
#                 offers_list = []
#                 page = 1
#                 while True:     # check all others offers in NEW filter
#                     url = retry_url(tour_filter, page, **offer)     # NEW filter
#                     logger.info(url)
#                     res = requests.get(
#                         url=url,
#                         headers=headers)
#                     offers_list += (res.json()['offers'])
#                     if res.json()['has_more_pages']:
#                         page += 1
#                     else:
#                         break
#                 # print(offers_list)
#                 # exit()
#                 for offer_ in offers_list:      # try to find offer WITH flights
#                     if has_flights_info(offer['key'], tour_filter.user.chat_id):
#                         tour_callback = detail_tour_info(offer['key'], tour_filter.user.chat_id)
#                         break       # break search loop when find offer WITH flights
#
#             if tour_callback:   # if offer WITH flights
#                 # tour_detail, _ = TourDetail.objects.get_or_create(key=offer['key'])
#                 tour_detail, _ = TourDetail.objects.get_or_create(key=offer['key'],
#                                                                   tour_id=tour_callback['tour_id'])
#                 tour_detail.order_int = offer['prices']['1']
#                 tour_detail.users.add(tour_filter.user)
#                 tour_detail.filters.add(tour_filter)
#                 tour_detail.save()
#                 logger.info(tour_callback['key'])
#
#                 save_new_callback_to_db(tour_detail, tour_callback, tour_filter)
#             else:   # if offer NO flights
#                 pass
#         return True


def new_search(tour_filter): ########TODO
    """get tours list from API call"""
    unique_offers = gen_offers_list(tour_filter)
    if not unique_offers:
        return False
    else:
        for offer in unique_offers:
            logger.info(offer['key'])
            tour_callback = detail_tour_info(offer['key'], tour_filter.user.chat_id)
            tour_callback['prices'] = offer['prices']
            # tour_detail, _ = TourDetail.objects.get_or_create(key=offer['key'])
            tour_detail, _ = TourDetail.objects.get_or_create(key=offer['key'],
                                                              tour_id=tour_callback['tour_id'])
            tour_detail.order_int = offer['prices']['1']
            tour_detail.users.add(tour_filter.user)
            tour_detail.filters.add(tour_filter)
            tour_detail.save()
            save_new_callback_to_db(tour_detail, tour_callback, tour_filter)
        return True


def mega_check(tour_callback):
    """check api call for all ways and flights"""
    if tour_callback['flights']['to'] and tour_callback['flights']['from']:
        if tour_callback['flights']['to'][0]['place'] != 'no' and tour_callback['flights']['from'][0]['place'] != 'no':
            logger.info('WITH flights')
            return True
    else:
        logger.info('NO flights')
        return False


# def has_flights_info(tour_key, user_id):
#     """get detail info about tour"""
    # res = requests.get(
    #     url=f'https://api.ittour.com.ua/tour/flights/{tour_key}',
    #     headers=headers)
#     try:    # handling LIMIT
#         if res.json()['error_code'] in [108, 109]:
#             logger.error('ERROR', res.json()['error_code'])
#             text = _l(user_id, 'system4')
#             TBot.bot.send_message(user_id, text)
#             exit(0)
#     except:
#         if res.json()['to'] and res.json()['from']:
#             if res.json()['to'][0]['place'] != 'no' and res.json()['from'][0]['place'] != 'no':
#                 logger.info('WITH flights')
#                 return True
#         else:
#             logger.info('NO flights')
#             return False


def detail_tour_info(tour_key, user_id):
    """get detail info about tour"""
    res = requests.get(
        url=f'https://api.ittour.com.ua/tour/info/{tour_key}',
        headers=headers)
    try:    # handling LIMIT
        if res.json()['error_code'] in [108, 109]:
            logger.error('ERROR', res.json()['error_code'])
            text = _l(user_id, 'system4')
            TBot.bot.send_message(user_id, text)
            exit(0)
    except:
        return res.json()


def validate_tour(tour_key):
    """get info about tour"""
    res = requests.get(
        url=f'https://api.ittour.com.ua/tour/validate/{tour_key}',
        headers=headers)
    return res.json()['stop_sale'], res.json()['stop_flight'], res.json()['price_changed']


def save_new_callback_to_db(tour_detail, tour_callback, tour_filter):
    """save callback to db and visualize it"""
    if not tour_callback['flights']['to']:
        flights_to = 'N/A'
        place_to = 'N/A'
    else:
        flights_to = tour_callback['flights']['to'][0]['date_from']
        place_to = tour_callback['flights']['to'][0]['place']

    if not tour_callback['flights']['from']:
        flights_from = f'{tour_filter.date_from:%d.%m.%Y}'
        place_from = 'N/A'
    else:
        flights_from = tour_callback['flights']['from'][0]['date_from']
        place_from = tour_callback['flights']['from'][0]['place']

    if tour_filter.currency == '1':
        currency_text = ' y.e.'
    else:
        currency_text = ' UAH'

    tour_callback_, status = TourCallback.objects.get_or_create(tour=tour_detail,
                                                 flights=f"{flights_from} - {flights_to}",
                                                 duration_nights=tour_callback['duration'],
                                                 hotel=f"{tour_callback['hotel']} - {tour_callback['hotel_rating']}",
                                                 meal_type=tour_callback['meal_type'],
                                                 room_type=tour_callback['room_type'],
                                                 accomodation=tour_callback['accomodation'],
                                                 price=str(tour_callback['prices'][tour_filter.currency]) + currency_text,
                                                #  price=str(offer['prices'][tour_filter.currency]) + currency_text,
                                                 operator=tour_callback['operator'],
                                                 place_from=place_from,
                                                 place_to=place_to,
                                                 region={'id': tour_callback['region_id'],
                                                         'name': tour_callback['region']},
                                                 date_on=tour_callback['date_from'],
                                                 hotel_dict={'id': tour_callback['hotel_id'],
                                                             'name': tour_callback['hotel']},
                                                 )
    logger.info('STATUS', status)
    logger.info(f'{tour_callback_=}')
    return tour_callback_, status


def make_text(tour_callback_):
    """that make text from tour_callback_ to send user"""

    text = f"[Тур ID] {tour_callback_.tour.tour_id}\n"
    text += f"[Даты вылета-отлета] <b>{tour_callback_.flights}</b>\n"
    text += f"[Кол-во ночей] <b>{tour_callback_.duration_nights}</b>\n"
    text += f"[Название отеля и кол-во*] <b>{tour_callback_.hotel}*</b>\n"
    text += f"[Питание] {tour_callback_.meal_type}\n"
    text += f"[Тип номера] {tour_callback_.room_type}\n"
    text += f"[Тип размещения] {tour_callback_.accomodation}\n"
    text += f"[Стоимость тура] <b>{tour_callback_.price}</b>\n"
    text += f"[Оператор] <b>{tour_callback_.operator}</b>\n"
    text += f"[Места на рейсе туда] {tour_callback_.place_from}\n"
    text += f"[Места на рейсе обратно] {tour_callback_.place_to}\n"
    logger.info(text)
    return text


def check_api():
    """check API status"""
    res = requests.get(url='https://api.ittour.com.ua/module/params', headers=headers)
    try:
        if res.json()['error_code']:
            return False
    except:
        return True


def main_url(tour, page):
    """make main url to search"""
    url = f"https://api.ittour.com.ua/module/search-list?type=1&kind=1&only_standard_price=0&dynamic_packet=0& \
        country={tour.country['id']}& \
        adult_amount={tour.adult_amount}&child_amount={len(tour.child_amount)}&hotel_rating={tour.hotel_rating}& \
        night_from={tour.nights_from}&night_till={tour.nights_to}& \
        date_from={tour.date_from.strftime('%d.%m.%y')}&date_till={tour.date_till.strftime('%d.%m.%y')}& \
        from_city={tour.from_city}&currency={tour.currency}"
    if len(tour.child_amount) > 0:
        url += '&child_age=' + ':'.join([tour.child_amount[str(i)] for i in range(1, len(tour.child_amount) + 1)])
    if tour.to_city:
        url += f'&region={tour.to_city["id"]}'
    if tour.hotel:
        url += f'&hotel={tour.hotel["id"]}'
    if tour.meal_type:
        url += f'&meal_type={tour.meal_type}'

    url += f'&page={page}&items_per_page=1000&hotel_info=0'
    return url


def retry_url(tour_filter, page=1, **tour):
    """make retry url to search tours when it NO flights in first one"""
    url = f"https://api.ittour.com.ua/module/search-list?type=1&kind=1&only_standard_price=0&dynamic_packet=0& \
        country={tour['country_id']}& \
        adult_amount={tour['adult_amount']}&child_amount={tour['child_amount']}&hotel_rating={tour['hotel_rating']}& \
        night_from={tour['duration']}&night_till={tour['duration']}& \
        date_from={tour_filter.date_from.strftime('%d.%m.%y')}&date_till={tour_filter.date_from.strftime('%d.%m.%y')}& \
        from_city={tour_filter.from_city}&currency={tour['currency_id']}"
    if tour['child_amount'] > 0:
        url += '&child_age=' + ':'.join([tour_filter.child_amount[str(i)] for i in range(1, len(tour_filter.child_amount) + 1)])
    url += f'&region={tour["region_id"]}'
    url += f'&hotel={tour["hotel_id"]}'
    url += f'&meal_type={tour_filter.meal_type}'
    url += f'&page={page}&items_per_page=1000&hotel_info=0'
    return url


# 165.227.158.161