from django import forms

from .models import Promo


class PromoForm(forms.ModelForm):
    class Meta:
        model = Promo
        exclude = ('id',)

    def clean(self):
        title_len = len(self.cleaned_data.get('title'))
        text_len = len(self.cleaned_data.get('text'))

        if title_len + text_len > 1024:
            raise forms.ValidationError(
                    'Ограничение по количеству символов суммарно заголовка и текста - 1024 символа.'
                )

        return self.cleaned_data
