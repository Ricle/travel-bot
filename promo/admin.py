from django.contrib import admin

from .forms import PromoForm
from .models import Promo


@admin.register(Promo)
class PromoAdmin(admin.ModelAdmin):
    form = PromoForm
    list_display = ('title',)
