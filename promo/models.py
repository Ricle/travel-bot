import os
from django.db import models


class Promo(models.Model):
    """Promo model in db."""
    title = models.CharField('Название', max_length=255)
    text = models.TextField('Описание', max_length=1024)
    image = models.ImageField(
        'Изображение', upload_to='promo/', help_text='Изображение не должно превышать 4 Мб.', null=True, blank=True
    )

    def __str__(self):
        return f'{self.title}'

    def delete(self):
        if self.image:
            os.remove(self.image.path)
        super().delete()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['pk']
