from Otpusk.celery import app
from tbot.customer import CustomerBot


@app.task
def make_mailing(promo_id, users):
    """Mail chosen promo to chosen users."""
    CustomerBot().send_promo_mailing(promo_id, users)
