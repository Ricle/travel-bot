import os
from celery import Celery
# from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Otpusk.settings")
app = Celery('Otpusk')

app.config_from_object("django.conf:settings", namespace='CELERY')

# app.conf.beat_schedule = {
#     'periodic1': {
#         'task': 'administrator.tasks.hour_check_tours',
#         'schedule': crontab(minute=47, hour=14)
#     },
# }
app.autodiscover_tasks()

